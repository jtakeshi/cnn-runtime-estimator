#include <cassert>
#include <cmath>
#include <iostream>

using namespace std;

unsigned int layer = 0;
unsigned int activations = 0;
unsigned int bootstraps = 0;
unsigned int additions = 0;
unsigned int multiplications = 0;
unsigned int rotations = 0;

unsigned int current_depth = 0;

// For different libraries: change the slot count and depth
static const unsigned int SLOTS = (1 << 17) >> 1;
const unsigned int HEAAN15_DEPTH = 5;
const unsigned int HEAAN16_DEPTH = 6;
const unsigned int HEAAN17_DEPTH = 13;

const unsigned int activation_depth = 3;

unsigned int library_max_depth = HEAAN17_DEPTH;

inline int next_pow2(int val) { return pow(2, ceil(log(val) / log(2))); }

inline int neg_mod(int val, int mod) { return ((val % mod) + mod) % mod; }

/* This is ugly but very useful. */
typedef struct Metadata {
  /* Number of plaintext slots in a ciphertext */
  int32_t slot_count;
  /* Number of plaintext slots in a half ciphertext (since ciphertexts are a
   * two column matrix) */
  int32_t pack_num;
  /* Number of Channels that can fit in a half ciphertext */
  int32_t chans_per_half;
  /* Number of input ciphertexts for convolution (this may not match what the
   * client actually transmits) */
  int32_t inp_ct;
  /* Number of output ciphertexts */
  int32_t out_ct;
  /* Image and Filters metadata */
  int32_t image_h;
  int32_t image_w;
  int32_t image_size;
  int32_t inp_chans;
  int32_t filter_h;
  int32_t filter_w;
  int32_t filter_size;
  int32_t out_chans;
  /* How many total ciphertext halves the input and output take up */
  int32_t inp_halves;
  int32_t out_halves;
  /* How many Channels are in the last output or input half */
  int32_t out_in_last;
  int32_t inp_in_last;
  /* The modulo used when deciding which output channels to pack into a mask */
  int32_t out_mod;
  /* How many permutations of ciphertexts are needed to generate all
   * int32_termediate rotation sets */
  int32_t half_perms;
  /* Whether the last output half can fit repeats */
  bool last_repeats;
  /* The number of Channels in a single repeat. This may require padding
   * since the repeat must be a power of 2 */
  int32_t repeat_chans;
  /* The number of rotations for each ciphertext half */
  int32_t half_rots;
  /* The number of rotations for the last ciphertext half */
  int32_t last_rots;
  /* Total number of convolutions needed to generate all intermediate
   * rotations sets */
  int32_t convs;
  int32_t stride_h;
  int32_t stride_w;
  int32_t output_h;
  int32_t output_w;
  int32_t pad_t;
  int32_t pad_b;
  int32_t pad_r;
  int32_t pad_l;
} Metadata;

unsigned int fc_layer = 1;

void fc_online(const Metadata &data) {

  cout << "Starting FC layer " << fc_layer << endl;
  fc_layer++;

  // For each matrix ciphertext, rotate the input vector once and multiply +
  // add

  rotations += data.inp_ct;
  additions += data.inp_ct;
  multiplications += data.inp_ct;
  /*
  for (int ct_idx = 0; ct_idx < data.inp_ct; ct_idx++) {
    rotations++;
    additions++;
    multiplications++;
  }
  */


  // Rotate all partial sums together
  int upperbound = next_pow2(data.image_size);
  assert(data.inp_ct >= 0);
  assert(data.inp_ct < upperbound);
  for (int rot = data.inp_ct; rot < upperbound; rot *= 2) {
    additions++;
    rotations++;
  }

  return;
}

Metadata conv_metadata(int slot_count, int image_h, int image_w, int filter_h,
                       int filter_w, int inp_chans, int out_chans, int stride_h,
                       int stride_w, bool pad_valid) {
  // If using Output packing we pad image_size to the nearest power of 2

  int image_size = next_pow2(image_h * image_w);

  int filter_size = filter_h * filter_w;

  int pack_num = slot_count / 2;

  int chans_per_half = pack_num / image_size;

  int out_ct = ceil((float)out_chans / (2 * chans_per_half));

  // In input packing we create inp_chans number of ciphertexts that are the
  // size of the output. In output packing we simply send the input
  // ciphertext
  int inp_ct = ceil((float)inp_chans / (2 * chans_per_half));

  int inp_halves = ceil((float)inp_chans / chans_per_half);

  int out_halves = ceil((float)out_chans / chans_per_half);

  // The modulo is calculated per ciphertext instead of per half since we
  // should never have the last out_half wrap around to the first in the same
  // ciphertext (unless there's only one output ciphertext)
  int out_mod = out_ct * 2 * chans_per_half;

  // This should always be even unless the whole ciphertext fits in a single
  // half since for each ciphertext permutation you also have to flip the
  // columns to get full coverage.
  int half_perms =
      (out_halves % 2 != 0 && out_halves > 1) ? out_halves + 1 : out_halves;

  assert(out_chans > 0 && inp_chans > 0);
  // Doesn't currently support a channel being larger than a half ciphertext
  assert(image_size < (slot_count / 2));

  // Number of inp/out channels in the last half
  int out_in_last = (out_chans % chans_per_half)
                        ? (out_chans % (2 * chans_per_half)) % chans_per_half
                        : chans_per_half;
  int inp_in_last = (inp_chans % chans_per_half)
                        ? (inp_chans % (2 * chans_per_half)) % chans_per_half
                        : chans_per_half;

  // Pad repeating channels in last half to the nearest power of 2 to allow
  // for a log number of rotations
  int repeat_chans = next_pow2(out_in_last);

  // If the out_chans in the last half take up a quarter ciphertext or less,
  // and the last half is the only half in the last ciphertext, we can do
  // repeats with log # of rotations
  bool last_repeats =
      (out_in_last <= chans_per_half / 2) && (out_halves % 2 == 1);

  // If the input or output is greater than one half ciphertext it will be
  // tightly packed, otherwise the number of rotations depends on the max of
  // output or inp chans
  int half_rots = (inp_halves > 1 || out_halves > 1)
                      ? chans_per_half
                      : max(max(out_chans, inp_chans), chans_per_half);
  // If we have repeats in the last half, we may have less rotations
  int last_rots = (last_repeats) ? repeat_chans : half_rots;

  // If we have only have a single half than we can possibly do less
  // convolutions if that half has repeats. Note that if a half has repeats
  // (ie. less rotations for a convolution) but we still do the half_rots #
  // of rotations than we simply skip the convolution for the last half
  // appropiately
  int convs = (out_halves == 1) ? last_rots : half_perms * half_rots;

  // Calculate padding
  int output_h, output_w, pad_t, pad_b, pad_r, pad_l;

  if (pad_valid) {
    output_h = ceil((float)(image_h - filter_h + 1) / stride_h);
    output_w = ceil((float)(image_w - filter_w + 1) / stride_w);
    pad_t = 0;
    pad_b = 0;
    pad_r = 0;
    pad_l = 0;
  } else {
    output_h = ceil((float)image_h / stride_h);
    output_w = ceil((float)image_w / stride_w);
    // Total amount of vertical and horizontal padding needed
    int pad_h = max((output_h - 1) * stride_h + filter_h - image_h, 0);
    int pad_w = max((output_w - 1) * stride_w + filter_w - image_w, 0);
    // Individual side padding
    pad_t = floor((float)pad_h / 2);
    pad_b = pad_h - pad_t;
    pad_l = floor((float)pad_w / 2);
    pad_r = pad_w - pad_l;
  }

  Metadata data = {slot_count,   pack_num,   chans_per_half, inp_ct,
                   out_ct,       image_h,    image_w,        image_size,
                   inp_chans,    filter_h,   filter_w,       filter_size,
                   out_chans,    inp_halves, out_halves,     out_in_last,
                   inp_in_last,  out_mod,    half_perms,     last_repeats,
                   repeat_chans, half_rots,  last_rots,      convs,
                   stride_h,     stride_w,   output_h,       output_w,
                   pad_t,        pad_b,      pad_r,          pad_l};
  return data;
}

Metadata fc_metadata(int slot_count, int vector_len, int matrix_h) {
  Metadata data;
  data.slot_count = slot_count;
  data.image_h = 1;
  data.image_w = vector_len;
  data.image_size = vector_len;
  data.filter_h = matrix_h;
  data.filter_w = vector_len;
  data.filter_size = data.filter_h * data.filter_w;
  // How many rows of matrix we can fit in a single ciphertext
  data.pack_num = slot_count / next_pow2(data.filter_w);
  float den = data.pack_num? data.pack_num : slot_count / (float)next_pow2(data.filter_w);
  // How many total ciphertexts we'll need
  data.inp_ct = ceil((float)next_pow2(data.filter_h) / den);
  return data;
}

void HE_conv(const Metadata &data) {

  int output_size = data.convs * data.inp_ct;
  // cout << "conv_layer " << layer << " output_size " << output_size << endl;
  layer++;
  activations += output_size;

  assert(library_max_depth);
  assert(activation_depth);
  assert(library_max_depth > activation_depth);

  for (unsigned int i = 0; i < activation_depth; i++) {
    current_depth++;
    if (current_depth == library_max_depth) {
      current_depth = 0;
      bootstraps += output_size;
    }
  }

  // Multiply masks and add for each convolution
  // cout << "loop times [" << data.half_perms << "]\n\n";
  for (int perm = 0; perm < data.half_perms; perm++) {
    for (int ct_idx = 0; ct_idx < data.inp_ct; ct_idx++) {
      // The output channel the current ct starts from
      int out_base =
          ((perm / 2 + ct_idx) * 2 * data.chans_per_half) % data.out_mod;
      // If we're on the last output half, the first and last halves aren't
      // in the same ciphertext, and the last half has repeats, then only
      // convolve last_rots number of times
      bool last_out = ((out_base + data.out_in_last) == data.out_chans) &&
                      data.out_halves != 2;
      bool half_repeats = last_out && data.last_repeats;
      int total_rots = (half_repeats) ? data.last_rots : data.half_rots;
      // cout << "mul times  [" <<  total_rots * data.filter_size<< "]\n\n";

      for (int rot = 0; rot < total_rots; rot++) {
        for (int f = 0; f < data.filter_size; f++) {

          // Note that if a mask is zero this will result in a
          // 'transparent' ciphertext which SEAL won't allow by default.
          // This isn't a problem however since we're adding the result
          // with something else, and the size is known beforehand so
          // having some elements be 0 doesn't matter

          multiplications++;
          additions++;
        }
      }
    }
  }
  return;
}

void HE_output_rotations(const Metadata &data) {
  // Init the result vector to all 0

  // For each half perm, add up all the inside channels of each half
  for (int perm = 0; perm < data.half_perms; perm += 2) {
    int rot;
    // Can save an addition or so by initially setting the partials vector
    // to a convolution result if it's correctly aligned. Otherwise init to
    // all 0s
    if (data.inp_chans <= data.out_chans || data.out_chans == 1) {

      rot = 1;
    } else {

      rot = 0;
    }
    for (int ct_idx = 0; ct_idx < data.inp_ct; ct_idx++) {
      // The output channel the current ct starts from
      int out_base =
          ((perm / 2 + ct_idx) * 2 * data.chans_per_half) % data.out_mod;
      // Whether we are on the last input half
      bool last_in = (perm + ct_idx + 1) % (data.inp_ct) == 0;
      // If we're on the last output half, the first and last halves aren't
      // in the same ciphertext, and the last half has repeats, then do the
      // rotations optimization when summing up
      bool last_out = ((out_base + data.out_in_last) == data.out_chans) &&
                      data.out_halves != 2;
      bool half_repeats = last_out && data.last_repeats;
      int total_rots = (half_repeats) ? data.last_rots : data.half_rots;
      // cout << "total_rotate times[" << total_rots << "]\n\n";
      for (int in_rot = rot; in_rot < total_rots; in_rot++) {
        int conv_idx = perm * data.half_rots + in_rot;
        int rot_amt;
        // If we're on a repeating half the amount we rotate will be
        // different
        if (half_repeats)
          rot_amt = -neg_mod(-in_rot, data.repeat_chans) * data.image_size;
        else
          rot_amt = -neg_mod(-in_rot, data.chans_per_half) * data.image_size;

        // Do the same for the column swap if it exists
        if (data.half_perms > 1) {
          if (rot_amt != 0)
            rotations += 1;
          additions += 1;
        }
        if (rot_amt != 0)
          rotations += 1;
        additions += 1;
      }
      // Add up a repeating half
      if (half_repeats) {
        // cout << "need to run add up a repeating half]\n\n";
        // If we're on the last inp_half then we might be able to do
        // less rotations. We may be able to find a power of 2 less
        // than chans_per_half that contains all of our needed repeats
        int size_to_reduce;
        if (last_in) {
          int num_repeats = ceil((float)data.inp_in_last / data.repeat_chans);
          //  We round the repeats to the closest power of 2
          int effective_repeats;
          // When we rotated in the previous loop we cause a bit of overflow
          // (one extra repeat_chans worth). If the extra overflow fits
          // into the modulo of the last repeat_chan we can do one
          // less rotation
          if (data.repeat_chans * num_repeats % data.inp_in_last ==
              data.repeat_chans - 1)
            effective_repeats = pow(2, ceil(log(num_repeats) / log(2)));
          else
            effective_repeats = pow(2, ceil(log(num_repeats + 1) / log(2)));
          // If the overflow ended up wrapping around then we simply
          // want chans_per_half as our size
          size_to_reduce =
              min(effective_repeats * data.repeat_chans, data.chans_per_half);
        } else
          size_to_reduce = data.chans_per_half;
        // Perform the actual rotations
        for (int in_rot = size_to_reduce / 2; in_rot >= data.repeat_chans;
             in_rot = in_rot / 2) {
          int rot_amt = in_rot * data.image_size;

          // Do the same for column swap if exists
          if (data.half_perms > 1) {
            if (rot_amt != 0)
              rotations += 1;
            additions += 1;
          }
          if (rot_amt != 0)
            rotations += 1;
          additions += 1;
        }
      }
      // The correct index for the correct ciphertext in the final output
      int out_idx = (perm / 2 + ct_idx) % data.out_ct;
      if (perm == 0) {
        // The first set of convolutions is aligned correctly

        if (data.out_halves == 1 && data.inp_halves > 1) {
          // If the output fits in a single half but the input
          // doesn't, add the two columns
          // cout << "rotate first columns_in place if aligned correctly\n\n";

          rotations += 1;
          additions += 1;
        }
        // Do the same for column swap if exists and we aren't on a repeat
        if (data.half_perms > 1 && !half_repeats) {
          rotations += 1;
          additions += 1;
        }
      } else {
        // Rotate the output ciphertexts by one and add

        additions += 1;
        // If we're on a tight half we add both halves together and
        // don't look at the column flip
        if (half_repeats) {
          rotations += 1;
          additions += 1;
        } else if (data.half_perms > 1) {
          rotations += 1;
          additions += 1;
        }
      }
    }
  }

  return;
}

unsigned int conv_layer = 1;

void server_conv_online(const Metadata *data) {

  cout << "Starting conv layer: " << conv_layer << endl;
  conv_layer++;

  HE_conv(*data);
  HE_output_rotations(*data);
}
void conv(int image_h, int image_w, int filter_h, int filter_w, int inp_chans,
          int out_chans, int stride, bool pad_valid) {
  Metadata data =
      conv_metadata(SLOTS, image_h, image_w, filter_h, filter_w, inp_chans,
                    out_chans, stride, stride, pad_valid);
  server_conv_online(&data);
}

void server_fc_online(const Metadata *data) { fc_online(*data); }

void fc(int vector_len, int matrix_h) {
  Metadata data = fc_metadata(SLOTS, vector_len, matrix_h);
  server_fc_online(&data);
}

int main(int argc, char **argv) {

  // Resnet50
  /*
conv(32, 32, 7, 7, 3, 64, 1, 0);
conv(16, 16, 1, 1, 64, 64, 1, 0);
conv(8, 8, 3, 3, 64, 64, 1, 0);
conv(8, 8, 1, 1, 64, 256, 1, 0);
conv(8, 8, 1, 1, 256, 256, 1, 0);
conv(8, 8, 1, 1, 256, 64, 1, 0);
conv(8, 8, 3, 3, 64, 64, 1, 0);
conv(8, 8, 1, 1, 64, 256, 1, 0);
conv(8, 8, 1, 1, 256, 64, 1, 0);
conv(8, 8, 3, 3, 64, 64, 1, 0);
conv(8, 8, 1, 1, 64, 256, 1, 0);
conv(8, 8, 1, 1, 256, 128, 1, 0);
conv(8, 8, 3, 3, 128, 128, 1, 0);
conv(4, 4, 1, 1, 128, 512, 1, 0);
conv(4, 4, 1, 1, 512, 512, 1, 0);
conv(4, 4, 1, 1, 512, 128, 1, 0);
conv(4, 4, 3, 3, 128, 128, 1, 0);
conv(4, 4, 1, 1, 128, 512, 1, 0);
conv(4, 4, 1, 1, 512, 128, 1, 0);
conv(4, 4, 3, 3, 128, 128, 1, 0);
conv(4, 4, 1, 1, 128, 512, 1, 0);
conv(4, 4, 1, 1, 512, 128, 1, 0);
conv(4, 4, 3, 3, 128, 128, 1, 0);
conv(4, 4, 1, 1, 128, 512, 1, 0);
conv(4, 4, 1, 1, 512, 256, 1, 0);
conv(4, 4, 3, 3, 256, 256, 1, 0);
conv(2, 2, 1, 1, 256, 1024, 1, 0);
conv(2, 2, 1, 1, 1024, 1024, 1, 0);
conv(2, 2, 1, 1, 1024, 256, 1, 0);
conv(2, 2, 3, 3, 256, 256, 1, 0);
conv(2, 2, 1, 1, 256, 1024, 1, 0);
conv(2, 2, 1, 1, 1024, 256, 1, 0);
conv(2, 2, 3, 3, 256, 256, 1, 0);
conv(2, 2, 1, 1, 256, 1024, 1, 0);
conv(2, 2, 1, 1, 1024, 256, 1, 0);
conv(2, 2, 3, 3, 256, 256, 1, 0);
conv(2, 2, 1, 1, 256, 1024, 1, 0);
conv(2, 2, 1, 1, 1024, 256, 1, 0);
conv(2, 2, 3, 3, 256, 256, 1, 0);
conv(2, 2, 1, 1, 256, 1024, 1, 0);
conv(2, 2, 1, 1, 1024, 256, 1, 0);
conv(2, 2, 3, 3, 256, 256, 1, 0);
conv(2, 2, 1, 1, 256, 1024, 1, 0);
conv(2, 2, 1, 1, 1024, 512, 1, 0);
conv(2, 2, 3, 3, 512, 512, 1, 0);
conv(1, 1, 1, 1, 512, 2048, 1, 0);
conv(1, 1, 1, 1, 2048, 2048, 1, 0);
conv(1, 1, 1, 1, 2048, 512, 1, 0);
conv(1, 1, 3, 3, 512, 512, 1, 0);
conv(1, 1, 1, 1, 512, 2048, 1, 0);
conv(1, 1, 1, 1, 2048, 512, 1, 0);
conv(1, 1, 3, 3, 512, 512, 1, 0);
conv(1, 1, 1, 1, 512, 2048, 1, 0);
fc(2048, 1000);
*/

  // VGG16
  /*  
  conv(32, 32, 3, 3, 3, 64, 1, 0);
  conv(32, 32, 3, 3, 64, 64, 1, 0);
  conv(32, 32, 3, 3, 64, 128, 1, 0);
  conv(16, 16, 3, 3, 128, 128, 1, 0);
  conv(16, 16, 3, 3, 128, 256, 1, 0);
  conv(8, 8, 3, 3, 256, 256, 1, 0);
  conv(8, 8, 3, 3, 256, 256, 1, 0);
  conv(8, 8, 3, 3, 256, 512, 1, 0);
  conv(4, 4, 3, 3, 512, 512, 1, 0);
  conv(4, 4, 3, 3, 512, 512, 1, 0);
  conv(4, 4, 3, 3, 512, 512, 1, 0);
  conv(2, 2, 3, 3, 512, 512, 1, 0);
  conv(2, 2, 3, 3, 512, 512, 1, 0);
  fc(25088, 4096);
  fc(4096, 4096);
  fc(4096, 1000);
  */

  // mobilenet
  

  conv( 32, 32, 3, 3, 3, 64, 1, 0);
conv(16,16,3,3,32,32,1, 0);
conv(16,16,1,1,32,16,1, 0);
conv(16,16,1,1,16,96,1, 0);
conv(16,16,3,3,96,96,1, 0);
conv(8,8,1,1,96,24,1, 0);
conv(8,8,1,1,24,144,1, 0);
conv(8,8,3,3,144,144,1, 0);
conv(8,8,1,1,144,24,1, 0);
conv(8,8,1,1,24,144,1, 0);
conv(8,8,3,3,144,144,1, 0);
conv(4,4,1,1,144,32,1, 0);
conv(4,4,1,1,32,192,1, 0);
conv(4,4,3,3,192,192,1, 0);
conv(4,4,1,1,192,32,1, 0);
conv(4,4,1,1,32,192,1, 0);
conv(4,4,3,3,192,192,1, 0);
conv(4,4,1,1,192,32,1, 0);
conv(4,4,1,1,32,192,1, 0);
conv(4,4,3,3,192,192,1, 0);
conv(2,2,1,1,192,64,1, 0);
conv(2,2,1,1,64,384,1, 0);
conv(2,2,3,3,384,384,1, 0);
conv(2,2,1,1,384,64,1, 0);
conv(2,2,1,1,64,384,1, 0);
conv(2,2,3,3,384,384,1, 0);
conv(2,2,1,1,384,64,1, 0);
conv(2,2,1,1,64,384,1, 0);
conv(2,2,3,3,384,384,1, 0);
conv(2,2,1,1,384,64,1, 0);
conv(2,2,1,1,64,384,1, 0);
conv(2,2,3,3,384,384,1, 0);
conv(2,2,1,1,384,96,1, 0);
conv(2,2,1,1,96,576,1, 0);
conv(2,2,3,3,576,576,1, 0);
conv(2,2,1,1,576,96,1, 0);
conv(2,2,1,1,96,576,1, 0);
conv(2,2,3,3,576,576,1, 0);
conv(2,2,1,1,576,96,1, 0);
conv(2,2,1,1,96,576,1, 0);
conv(2,2,3,3,576,576,1, 0);
conv(1,1,1,1,576,160,1, 0);
conv(1,1,1,1,160,960,1, 0);
conv(1,1,3,3,960,960,1, 0);
conv(1,1,1,1,960,160,1, 0);
conv(1,1,1,1,160,960,1, 0);
conv(1,1,3,3,960,960,1, 0);
conv(1,1,1,1,960,160,1, 0);
conv(1,1,1,1,160,960,1, 0);
conv(1,1,3,3,960,960,1, 0);
conv(1,1,1,1,960,320,1, 0);
conv(1,1,1,1,320,1280,1, 0);
fc(1280,1000);


  cout << "activations " << activations << endl;
  cout << "bootstraps " << bootstraps << endl;
  cout << "additions " << additions << endl;
  cout << "multiplications " << multiplications << endl;
  cout << "rotations " << rotations << endl;

  return 0;
}
