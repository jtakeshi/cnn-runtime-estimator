#!/usr/bin/python3

# Conv. and activations counted and calculated separately
# Function name is misnomer, as this now also counts FC time
def conv_time(network, library):
  return network["plain-additions"]*library["plain-addition"] + network["plain-multiplications"]*library["plain-multiplication"] + network["rotations"]*library["rotation"]

def activation_time(network, library, activation):
  return network["activations"]*(activation["hom-additions"]*library["hom-addition"] + activation["hom-multiplications"]*library["hom-multiplication"] + activation["plain-multiplications"]*library["plain-multiplication"])

# network["pools"] and network["fc-mult"] should be iterables
# network["pools"] will have elements with 2 elements, representing the output size and filter size of each layer
def pooling_time(network, library):
  ret = 0.0
  for x in list(network["pools"]):
    pool_output_size = x[0]
    pool_filter_size = x[1]
    ret += pool_output_size * (pool_filter_size*library["hom-addition"] + library["plain-multiplication"])
  return ret
  
  
# network["fc-mult"] is an iterable, describing the size of every FC layer
'''
def fc_time(network, library, activation):
  return network["fc-rotations"]*library["rotation"] + network["fc-plain-additions"]*library["plain-addition"] + network["fc-plain-multiplications"]*library["plain-multiplication"]
'''  
  
'''
  ret = 0.0
  for fc_size in list(network["fc-mult"]):
    ret += fc_size * (fc_size * library["plain-multiplication"] + ((fc_size-1)* library["plain-addition"] ))
    # Add in activation time here
    ret += fc_size*fc_size*(activation["hom-addition"]*library["hom-addition"] + activation["hom-multiplication"]*library["hom-multiplication"])
  return ret
'''
  


  
# Networks  
resnet50_15_5 = dict()
resnet50_15_5["plain-additions"] = 86555
resnet50_15_5["plain-multiplications"] = 59968
resnet50_15_5["activations"] = 26560 
resnet50_15_5["rotations"] = 26697
resnet50_15_5["pools"] = [(56**2, 3**2), (2048, 3**2)]

resnet50_16_6 = dict()
resnet50_16_6["plain-additions"] = 86502
resnet50_16_6["plain-multiplications"] = 59904
resnet50_16_6["activations"] = 26560 
resnet50_16_6["rotations"] = 26646
resnet50_16_6["pools"] = [(56**2, 3**2), (2048, 3**2)]

resnet50_17_13 = dict()
resnet50_17_13["plain-additions"] = 86479
resnet50_17_13["plain-multiplications"] = 59872
resnet50_17_13["activations"] = 26560 
resnet50_17_13["rotations"] = 26624
resnet50_17_13["pools"] = [(56**2, 3**2), (2048, 3**2)]


VGG_POOL_SIZE=4
vgg_15_5 = dict()
vgg_15_5["plain-additions"] = 61301
vgg_15_5["plain-multiplications"] = 56128
vgg_15_5["rotations"] = 14593
vgg_15_5["pools"] = [(64 * 16 * 16, VGG_POOL_SIZE), (128*8*8, VGG_POOL_SIZE), (256*4*4, VGG_POOL_SIZE), (512*2*2, VGG_POOL_SIZE), (512, VGG_POOL_SIZE), (512*7*7, VGG_POOL_SIZE)]
vgg_15_5["activations"] = 5184

vgg_16_6 = dict()
vgg_16_6["plain-additions"] = 48897
vgg_16_6["plain-multiplications"] = 44480
vgg_16_6["rotations"] = 9143
vgg_16_6["pools"] = []
vgg_16_6["activations"] = 4416

vgg_17_13 = dict()
vgg_17_13["plain-additions"] = 44616
vgg_17_13["plain-multiplications"] = 40384
vgg_17_13["rotations"] = 6599
vgg_17_13["pools"] = []
vgg_17_13["activations"] = 4224


mobilenet_15_5 = dict()
mobilenet_15_5["plain-additions"] = 125894
mobilenet_15_5["plain-multiplications"] = 102288
mobilenet_15_5["rotations"] = 23711
mobilenet_15_5["pools"] = [(1024, 7)]
mobilenet_15_5["activations"] = 23568

mobilenet_16_6 = dict()
mobilenet_16_6["plain-additions"] = 124555
mobilenet_16_6["plain-multiplications"] = 101072
mobilenet_16_6["rotations"] = 23529
mobilenet_16_6["pools"] = [(1024, 7)]
mobilenet_16_6["activations"] = 23440

mobilenet_17_13 = dict()
mobilenet_17_13["plain-additions"] = 124528
mobilenet_17_13["plain-multiplications"] = 101040
mobilenet_17_13["rotations"] = 23503
mobilenet_17_13["pools"] = [(1024, 7)]
mobilenet_17_13["activations"] = 23440


#Activation function
deg6 = dict()
deg6["depth"] = 3
deg6["hom-additions"] = 6
deg6["hom-multiplications"] = 5
deg6["plain-multiplications"] = 6


#Libraries
heaan15 = dict()
heaan15["bootstrap"] = 4.17E+10
heaan15["bootstrap-depth"] = 5 
heaan15["plain-addition"] = 1.28E+06
heaan15["plain-multiplication"] = 1.50E+07
heaan15["rotation"] = 5.63E+08
heaan15["hom-addition"] = 1.70E+06
heaan15["hom-multiplication"] = 1.23E+08

heaan16 = dict()
heaan16["bootstrap"] = 17287800000
heaan16["bootstrap-depth"] = 6 
heaan16["plain-addition"] = 3.58E+06
heaan16["plain-multiplication"] = 4.95E+07
heaan16["rotation"] = 1.23E+09
heaan16["hom-addition"] = 4.61E+06
heaan16["hom-multiplication"] = 2.78E+08

heaan17 = dict()
heaan17["bootstrap"] = 1.24E+11
heaan17["bootstrap-depth"] = 13 
heaan17["plain-addition"] = 2.54E+07
heaan17["plain-multiplication"] = 3.90E+08
heaan17["rotation"] = 1.24E+10
heaan17["hom-addition"] = 3.10E+07
heaan17["hom-multiplication"] = 2.45E+09

#Counts number of bootstraps needed, and multiplies by the runtime
def bootstrapping_time(network, library, activation):
  ret_time = library["bootstrap"]
  if network == resnet50_15_5 and library == heaan15:
    ret_time *= 14976
  elif network == resnet50_16_6 and library == heaan16:
    ret_time *= 12992 
  elif network == resnet50_17_13 and library == heaan17:
    ret_time *= 4416
  elif network == vgg_15_5 and library == heaan15:
    ret_time *= 2816  
  elif network == vgg_16_6 and library == heaan16:
    ret_time *= 2048
  elif network == vgg_17_13 and library == heaan17:
    ret_time *= 1280
  elif network == mobilenet_15_5 and library == heaan15:
    ret_time *= 15168
  elif network == mobilenet_16_6 and library == heaan16:
    ret_time *= 12544
  elif network == mobilenet_17_13 and library == heaan17:
    ret_time *= 6464
  else:
    assert False and "Bad network/library combo!"
  return ret_time
  
def runtime(network, library, activation):
  return conv_time(network, library) + activation_time(network, library, activation) + pooling_time(network, library) + bootstrapping_time(network, library, activation)  

def main():
  print("Resnet,HEAAN,15," + str(runtime(resnet50_15_5, heaan15, deg6)))
  print("Resnet,HEAAN,16," + str(runtime(resnet50_16_6, heaan16, deg6)))
  print("Resnet,HEAAN,17," + str(runtime(resnet50_17_13, heaan17, deg6)))
  
  print("VGG,HEAAN,15," + str(runtime(vgg_15_5, heaan15, deg6)))
  print("VGG,HEAAN,16," + str(runtime(vgg_16_6, heaan16, deg6)))
  print("VGG,HEAAN,17," + str(runtime(vgg_17_13, heaan17, deg6)))
  
  print("Mobilenet,HEAAN,15," + str(runtime(mobilenet_15_5, heaan15, deg6)))
  print("Mobilenet,HEAAN,16," + str(runtime(mobilenet_16_6, heaan16, deg6)))
  print("Mobilenet,HEAAN,17," + str(runtime(mobilenet_17_13, heaan17, deg6)))
  
  return 
  
if __name__ == '__main__':
  main()  
  
